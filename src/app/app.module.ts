import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from './app.component';
import { FirstComponent } from './first/first.component';
import { ExoComponent } from './exoTemplate/exo.component';
import { StructureComponent } from './structure/structure.component';
import { ListComponent } from './list/list.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ObservableComponent } from './observable/observable.component';


const routes: Routes = [
  {path: 'first', component: FirstComponent},
  {path: 'list', component: ListComponent},
  {path: 'exo', component: ExoComponent},
  {path: 'structure', component: StructureComponent},
  {path: 'observable', component: ObservableComponent},
  {path: '', redirectTo: '/first', pathMatch: 'full'},
  {path: '**', component: NotFoundComponent}
  
];

@NgModule({
  declarations: [
    AppComponent,
    FirstComponent,
    ExoComponent,
    StructureComponent,
    ListComponent,
    NotFoundComponent,
    ObservableComponent
    
    
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      routes,
      // { enableTracing: true } // <-- debugging purposes only
    ),
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
