import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Picture } from '../entity/picture';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AjaxService {
  private url = 'http://localhost:8080/api/picture';
  
  constructor(private http:HttpClient) { }

  findAllPicture(): Observable<Picture[]> {
    // console.log('from service');
return this.http.get<Picture[]>(this.url);
  }
}
