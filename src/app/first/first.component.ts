import { Component } from "@angular/core";

@Component({                                //ne pas oublier de le declarer dans le module(app.module.ts)dans declarartions
  templateUrl: './first.component.html' ,
  selector: 'simplon-first'
})
export class FirstComponent {
  public maVariable = 'bloup';
  imgSrc = "https://ssl.gstatic.com/ui/v1/icons/mail/rfr/logo_gmail_lockup_dark_1x.png";
  isHidden = false;

  tooglePara() {
    this.isHidden = !this.isHidden;
  }
}

