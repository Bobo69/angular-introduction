import { Component, OnInit } from '@angular/core';
import { Observable,from } from 'rxjs';
import { AjaxService } from '../service/ajax.service';
import { Picture } from '../entity/picture';

@Component({
  selector: 'app-observable',
  templateUrl: './observable.component.html',
  styleUrls: ['./observable.component.css']
})
export class ObservableComponent implements OnInit {
  pictures:Picture[] = [];

  constructor(private service:AjaxService) { }

  ngOnInit() {

    this.service.findAllPicture().subscribe(response => this.pictures = response);

    let observableArray = from(['ga', 'zo', 'bu', 'meu']);

    observableArray.subscribe( function(data) {
    console.log(data);
  },

  function(error) {
    console.log(error);
  },

  function() {
    console.log('finished');
  }
);

  let observableHomemade = Observable.create(function(observer) {
    for (let item of ['ga', 'zo', 'bu']) {
      observer.next(item);
    }
    observer.complete();
  });
  
// let observableHomemade = Observable.create(function(observer) {
//   observer.next('bloup');
//   observer.next('bloup');
//   observer.next('bloup');
//   observer.complete();
// });

observableHomemade.subscribe(data => console.log(data),
error => console.log(error),
() => console.log('finished')
);

  }
}
