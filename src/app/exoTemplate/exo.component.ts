import { Component } from "@angular/core";


@Component({
  templateUrl: './exo.component.html',
  selector: 'simplon-exo-first'
})

export class ExoComponent{
 public color = "black";

 changeColor(newColor) {

      this.color = newColor;
}
}
