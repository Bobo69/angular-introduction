import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'simplon-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {


  list = ["polux", "pimprenelle"];
  text = "";
  constructor() { }

  ngOnInit() {
  }
  addItem() {
    this.list.push(this.text);
  }
  rmItem(index:number) {
    this.list.splice(index,1);
  }
  changeOrder(index: number, direction: string) {
    if (direction == "down") {
      this.list.splice(index + 2, 0, this.list[index]);
      this.list.splice(index, 1);
    }

    else {
      this.list.splice(index - 1, 0, this.list[index]);
      this.list.splice(index + 1, 1);
    }
  }
}

